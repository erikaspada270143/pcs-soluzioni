#include "Intersector2D2D.h"
#include <cmath>
// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceParallelism = 1.0E-5;
    toleranceIntersection = 1.0E-7;
    matrixNomalVector.setZero();
    rightHandSide.setZero();
    pointLine.setZero();
    tangentLine.setZero();
}


Intersector2D2D::~Intersector2D2D()
{

}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(0) = planeNormal;   //matrixNomalVector (  N1x N2x N3x      right ( d1
                                              //                     0    0   0               0
    rightHandSide[0] = planeTranslation;         //                  0    0   0  )            0
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNomalVector.row(1) = planeNormal;   //matrixNomalVector (  N1x N1y N1z      right ( d1
                                              //                     N2x N2y N3z              d2
    rightHandSide[0] = planeTranslation;         //                  0    0   0  )            0
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    bool intersection = true;
    tangentLine = matrixNomalVector.row(0).cross(matrixNomalVector.row(1));  //sarebbe la t della retta che si crea intersecando i piano
    if(tangentLine.norm() > toleranceIntersection || tangentLine.norm() < -toleranceIntersection) // i piani si incontrano se il prodotto vettoriale tra le normasle è diverso da zero, cioe non sono parallale
    {
        matrixNomalVector.row(2) = tangentLine;   //matrixNomalVector (  N1x N1y N1z     ( x    = ( d1
                                                  //                      N2x N2y N3z      y    =   d2
                                                 //  N3 = N1 x N2         N3x N3y N3z  )   z )  =    0)

       pointLine = matrixNomalVector.fullPivHouseholderQr().solve(rightHandSide);  // trovo il punto P (x,y,z)

       intersectionType = LineIntersection;
       intersection = true;
    }
    else
    {
        if(abs(rightHandSide[0] - rightHandSide[1]) < toleranceParallelism) //cioè se la distanza tra ii piani è zero, coincidono
        {

            intersectionType = Coplanar;
            intersection = false;
        }
        else //i piani sono parallaleli tra loro
        {
            intersectionType = NoInteresection;
            intersection = false;
        }
    }

  return intersection;
}
