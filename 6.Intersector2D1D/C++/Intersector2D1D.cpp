#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceParallelism = 1.0E-7;
    toleranceIntersection = 1.0E-7;
    intersectionParametricCoordinate = 0.0;
}
Intersector2D1D::~Intersector2D1D()
{

}

// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
   planeNormalPointer = &planeNormal;
   planeTranslationPointer = &planeTranslation;

}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{

    lineOriginPointer =&lineOrigin;
    lineTangentPointer= &lineTangent;

}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    bool intersection = true;
    Vector3d x_0;

    double num = *planeTranslationPointer -  (*planeNormalPointer).transpose().dot((*lineOriginPointer)); //numeratore di s cioè n^t(x0 - origine retta) dove n^tx0 = d
    double den = (*planeNormalPointer).transpose().dot((*lineTangentPointer)); // denominatore s cioè n^t per direzione retta

    if(den > toleranceIntersection || den < -toleranceIntersection) //caso in cui si intersecano e il denominatore è diverso da zero
    {
        intersectionType = PointIntersection;
        intersection = true;
        intersectionParametricCoordinate = num/den; //trovo s

    }
    else
    {
        if(num > toleranceParallelism || num < -toleranceParallelism) //caso in cui non si intersecano e n^t(x_0 - y_0) diverso da zero
        {                                                             //numeratore diverso da zero e denominatore uguale a zero
            intersectionType = NoInteresection;
            intersection = false;
        }
        else                         //num=0 e den=0
        {                           // caso in cui la retta giace sul piano e n^t(x_0 - y_0)= 0
            intersectionType = Coplanar;
            intersection = false;

        }
    }

  return intersection;
}
Vector3d Intersector2D1D::IntersectionPoint()
{
    return (*lineOriginPointer + intersectionParametricCoordinate * *lineTangentPointer); //il punto di intersezione lo trovo sostituendo il valore di s nella retta
}
