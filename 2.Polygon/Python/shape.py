import math
class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a = a
        self.b = b
    def area(self):
        return self.a*self.b*math.pi



class Circle(Ellipse):
    def __init__(self, center: Point, radius: int):
        super(Ellipse, self).__init__()
        self.center= center
        self.radius= radius
    def area(self):
        return Ellipse.area(Ellipse(self.center, self.radius, self.radius))



class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1= p1
        self.p2 = p2
        self.p3 = p3

    def area(self):
        ab = math.sqrt(pow((self.p1.x-self.p2.x),2)+pow((self.p1.y-self.p2.y),2))
        bc = math.sqrt(pow((self.p2.x-self.p3.x),2)+pow((self.p2.y-self.p3.y),2))
        ca = math.sqrt(pow((self.p3.x-self.p1.x),2)+pow((self.p3.y-self.p1.y),2))
        p = (ab + bc + ca) / 2
        return math.sqrt(p * (p - ab) * (p - bc) * (p - ca))


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge
    def area(self):
        return (pow(self.edge, 2)*(math.sqrt(3)/4))

class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4
    def area(self):
        ab = math.sqrt(pow((self.p1.x - self.p2.x), 2) + pow((self.p1.y - self.p2.y), 2))
        bc = math.sqrt(pow((self.p2.x - self.p3.x), 2) + pow((self.p2.y - self.p3.y), 2))
        cd = math.sqrt(pow((self.p3.x - self.p4.x), 2) + pow((self.p3.y - self.p4.y), 2))
        da = math.sqrt(pow((self.p4.x - self.p1.x), 2) + pow((self.p4.y - self.p1.y), 2))
        diag = math.sqrt(pow((self.p1.x - self.p3.x), 2) + pow((self.p1.y - self.p3.y), 2))

        # divido il quadrilatero in due triangoli e uso la formula di erode

        spu = (ab + bc + diag) / 2
        spd = (da + cd + diag) / 2
        return (math.sqrt(spu * (spu - ab) * (spu - bc) * (spu - diag)) + math.sqrt(spd * (spd - da) * (spd - cd) * (spd - diag)))

        pass


class Parallelogram(Quadrilateral):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        super(Quadrilateral, self).__init__()
        self.p1 = p1
        self.p2 = p2
        self.p4 = p4
    def area(self):
        return Quadrilateral.area(Quadrilateral(self.p2,self.p1,self.p4,self.p1))



class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1= p1
        self.base = base
        self.height = height
    def area(self):
        return self.base*self.height


class Square(Rectangle):
    def __init__(self, p1: Point, edge: int):
        super(Rectangle,self).__init__()
        self.p1 = p1
        self.edge = edge
    def area(self):
        return Rectangle.area(Rectangle(self.p1,self.edge,self.edge))


