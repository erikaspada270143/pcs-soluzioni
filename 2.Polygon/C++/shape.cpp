#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

Point::Point(const double &x, const double &y)
{
    _x = x;
    _y = y;
}

Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _a = a;
    _b = b;
}

double Ellipse::Area() const
{
    return(M_PI * _a * _b);
}

Circle::Circle(const Point &center, const int &radius):Ellipse(center,radius,radius)
{
}

double Circle::Area() const
{
    return Ellipse::Area();
}

Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3) : _p1(p1), _p2(p2), _p3(p3){}

double Triangle::Area() const
{
    double ab, bc, ca,p;
    ab = sqrt(pow((_p1._x - _p2._x),2) + pow((_p1._y - _p2._y),2)); //trovo lati
    bc = sqrt(pow((_p2._x - _p3._x),2) + pow((_p2._y - _p3._y),2));
    ca = sqrt(pow((_p3._x - _p1._x),2) + pow((_p3._y - _p1._y),2));
    p = (ab + bc + ca)/2;
    return(sqrt(p*(p - ab)*(p - bc)*(p - ca))); //formula di erone
}

TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge):Triangle(p1,p1,p1)
{
    _p2._x = _p1._x - (0.5*edge);
    _p3._x = _p1._x + (0.5*edge);
    _p2._y = _p1._y - (0.5*edge*sqrt(3));
    _p3._y =  _p2._y;
}

double TriangleEquilateral::Area() const
{
    return Triangle::Area();
}
Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4): _p1(p1), _p2(p2), _p3(p3), _p4(p4){}

double Quadrilateral::Area() const
{
    double ab, bc, cd, da, diag, sp1,sp2;
    ab = sqrt(pow((_p1._x - _p2._x),2) + pow((_p1._y - _p2._y),2)); //trovo lati
    bc = sqrt(pow((_p2._x - _p3._x),2) + pow((_p2._y - _p3._y),2));
    cd = sqrt(pow((_p3._x - _p4._x),2) + pow((_p3._y - _p4._y),2));
    da = sqrt(pow((_p4._x - _p1._x),2) + pow((_p4._y - _p1._y),2));
    diag = sqrt(pow((_p1._x - _p3._x),2) + pow((_p1._y - _p3._y),2));
    //divido il quadrilatero in due triangoli e uso la formula di erode
    sp1= (ab + bc + diag)/2;
    sp2= (da + cd + diag)/2;

    return(sqrt(sp1*(sp1 - ab)*(sp1 - bc)*(sp1 - diag))+
           sqrt(sp2*(sp2 - da)*(sp2 - cd)*(sp2 - diag)));
}

Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):Quadrilateral(p2,p1,p4,p1) {}

double Parallelogram::Area() const
{
    return Quadrilateral::Area();
}

Rectangle::Rectangle(const Point &p1, const int &base, const int &height) //potrei usare le funzioni di area di parallelogramma o quadrilatero, ma risulterebbe piui difficile risistemare i vertici, quindi piu smart calcolarsi direttamente l'area
{
    _base= base;
    _height = height;
}

double Rectangle::Area() const
{
    return(_base*_height);
}

Square::Square(const Point &p1, const int &edge) :Rectangle(p1, edge,edge)
{

}

double Square::Area() const
{
    return Rectangle ::Area();
}



}
