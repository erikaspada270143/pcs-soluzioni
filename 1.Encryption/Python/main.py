import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text


def importText(inputFilePath):
    file = open(inputFilePath,'r')
    text = file.readlines()[0] #leggo la prima riga dal file
    file.close()
    return True, text

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text
def encrypt(text, password):
    l = len(password)
    encryptedText= ''
    for i in range(0, len(text)):
        temp = int(ord(text[i]) + ord(password[i % l])) #converto ogni carattere in ascii
        if temp > 126: #solo 126 caratteri in ascii
            temp = temp - 95 #riporto a 32 che è il primo caratttere stampabile

        encryptedText = encryptedText + str(chr(temp)) #converto l'ascii in carattere e poi creo una mini stringa con quel carattere e poi sommmo tutte le mini stringhe



    return True, encryptedText
# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text
def decrypt(text, password):
    l = len(password)
    decryptedText =''
    for i in range(0, len(text)):
        temp = int(ord(text[i]) - ord(password[i % l]))  # converto ogni carattere in ascii
        if temp < 32:  # 32 che è il primo caratttere stampabile
            temp = temp + 95  # riporto a 32 che è il primo caratttere stampabile

        decryptedText = decryptedText + str(chr(temp))  # converto l'ascii in carattere e poi creo una mini stringa con quel carattere e poi sommmo tutte le mini stringhe


    return True, decryptedText

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text=", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result= ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result= ", decryptedText)
