import numpy as np
#inserisco tolleranza
tolerance= 1.0E-7


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __eq__(self, point):  #operatore booleano ==
        if isinstance(point,Point):
           return self.x==point.x and self.y==point.y

    def __lt__(self, other):
        if abs(self.x - other.x) > tolerance:
            return self.x > other.x
        else:
            return self.y > other.y



class Intersector1D1D :
    def __init__(self):
        self.resultParametricCoordinates = [0.0,0.0] # s segmenti(posizione 0: segmento, posizione 1: lato)
        self.originFirstSegment = Point(0,0) # x0, y0
        self.rightHandSide=[0.0,0.0]# in posizione 0 x_1 - x_0, in pos 1 y_1 - y_0
        self.matrixTangentVector=[[0,0],[0,0]]# t segmenti(direzioni) [[]]

    def SetFirstSegment(self,origin,end):
        self.matrixTangentVector[0][0]= (end[0] - origin[0])
        self.matrixTangentVector[1][0] = (end[1] - origin[1])
        self.originFirstSegment = Point(origin[0],origin[1])

    def SetSecondSegment(self,origin,end):
        self.matrixTangentVector[0][1] = (origin[0] - end[0])
        self.matrixTangentVector[1][1] = (origin[1] - end[1])
        self.rightHandSide[0] = origin[0] - self.originFirstSegment.x
        self.rightHandSide[1] = origin[1] - self.originFirstSegment.y


    def ComputeIntersection(self): #verifica se c'è intersezione tra il primo segmento (prolungato in retta) e il secondo segmento
        determinant = np.linalg.det(self.matrixTangentVector) # prodotto vettoriale t segmenti
        intersection = False
        check = tolerance * tolerance *np.sqrt(self.matrixTangentVector[0][0]*self.matrixTangentVector[0][0]+ self.matrixTangentVector[1][0]*self.matrixTangentVector[1][0])*np.sqrt(self.matrixTangentVector[0][1]*self.matrixTangentVector[0][1]+ self.matrixTangentVector[1][1]*self.matrixTangentVector[1][1])
        if determinant * determinant >= check: #non sono paralleli
            solverMatrix = np.linalg.inv(self.matrixTangentVector)
            self.resultParametricCoordinates = np.dot(solverMatrix,self.rightHandSide)
            if self.resultParametricCoordinates[1] >-tolerance:
                if self.resultParametricCoordinates[1] - 1.0 < tolerance :
                    intersection = True

        # studio i casi limite
        if abs(self.resultParametricCoordinates[1]) < tolerance :
            self.resultParametricCoordinates[1] = 0.0 # inzio lato
        else:
            if abs(self.resultParametricCoordinates[1] - 1.0) < tolerance :
                self.resultParametricCoordinates[1] = 1.0 #fine lato

        return intersection

    def PointOfIntersection(self,origin,end):
        self.intersersectionPoint = Point(0, 0)  # punto di intersezione segmenti
        self.intersersectionPoint.x = (1 - self.resultParametricCoordinates[1]) * origin[0] + self.resultParametricCoordinates[1] * end[0]
        self.intersersectionPoint.y = (1 - self.resultParametricCoordinates[1]) * origin[1] + self.resultParametricCoordinates[1] * end[1]
        return self.intersersectionPoint




class Polygon():
    def __init__(self):
        self.segment=[]  # punti segmento
        # tutto in senso antiorario
        self.points = [] # punti poligono
        self.polygonVertices = [] #vertici poligono
        self.intersections = [] # punti di intersezione + punti segmento se interni al poligono
        self.intersectionVertices=[] #vertici di intersections
        self.perimeterPoints = [] # punti sul perimetro del poligono
        self.perimeterVertices = [] #vertici perimeterPoints
        self.newPoints = [] #tutti i punti dopo il taglio
        self.cuttedPolygons = [] #poligoni tagliati(vertici)

    def CreatePolygon(self, polygonPoints,vertices,segmentPoints):
        for i in range(len(vertices)):
            self.polygonVertices.append(vertices[i])
            self.points.append(Point(polygonPoints[i][0], polygonPoints[i][1]))
            self.newPoints.append(self.points[i]) # aggiungo gia i vertici del poligono ai miei nuovi punti

        self.segment.append(Point(segmentPoints[0][0], segmentPoints[0][1]))
        self.segment.append(Point(segmentPoints[1][0], segmentPoints[1][1]))

    def PointOnLine(self, firstEdge: Point, secondEdge: Point,
                    segmentEdge: Point):  # per verificare se un punto di intersezione appartiene a un lato
        found = False
        m = 0
        x = firstEdge.x - secondEdge.x
        type=0

        if x<0:   #lato orientato da sx a dx
            type=1
        if x>0:   #lato 0rientato da dx verso sx
            type=-1
        if x==0:  #lato verticale
            type=0

        if type ==0:
            if abs(segmentEdge.x - firstEdge.x) < tolerance:  #controllo che abbia la stessa x dei punti del lato
                if firstEdge.y <= secondEdge.y : # controllo che la y di segmentEdge sia compresa tra le y di firstEdge e secondEdge
                    if (firstEdge.y-segmentEdge.y) <=tolerance and (segmentEdge.y-secondEdge.y) <= tolerance :
                        found = True
                else:
                    if (secondEdge.y-segmentEdge.y) <=tolerance and (segmentEdge.y-firstEdge.y) <= tolerance :
                        found = True

        if type == 1 :
            m = (secondEdge.y - firstEdge.y) / (secondEdge.x - firstEdge.x) #coeff. angolare
            if abs((segmentEdge.y) - (firstEdge.y + m * (segmentEdge.x - firstEdge.x))) < tolerance :  # verifico che punto appartiene a retta
                if (firstEdge.x - segmentEdge.x)<= tolerance and (segmentEdge.x - secondEdge.x) <= tolerance : ## controllo che la x di segmentEdge sia compresa tra le x di firstEdge e secondEdge
                    found = True

        if type ==-1 :
            m = (secondEdge.y - firstEdge.y) / (secondEdge.x - firstEdge.x) #coeff. angolare
            if abs((segmentEdge.y) - (firstEdge.y + m * (segmentEdge.x - firstEdge.x))) < tolerance : # verifico che punto appartiene a retta
                if ((secondEdge.x - segmentEdge.x)<= tolerance and (segmentEdge.x - firstEdge.x) <= tolerance):  ## controllo che la x di segmentEdge sia compresa tra le x di firstEdge e secondEdge
                    found = True


        return found


    def Intersection(self):  #calcola le intersezioni segmento/lati poligono
        origin= [0.0,0.0]
        end= [0.0,0.0]
        val = Intersector1D1D() #oggetto della classe intersector per richiamare le sue funzioni
        flag = 0
        origin[0] = self.segment[0].x
        origin[1] = self.segment[0].y
        end[0] = self.segment[1].x
        end[1] = self.segment[1].y
        val.SetFirstSegment(origin, end) # setto il segmento
        for i in range(len(self.points)):
            origin[0] = self.points[i].x
            origin[1] = self.points[i].y
            end[0] = self.points[(i + 1) % len(self.points)].x
            end[1] = self.points[(i + 1) % len(self.points)].y
            val.SetSecondSegment(origin, end)# origin e end sono due vertici consecutivi del poligono(lato)
            if val.ComputeIntersection() == True :
                temp = val.PointOfIntersection(origin, end) #punto di intersezione segmento / lato
                if len(self.intersections) > 0: #controllo che il punto non sia già stato trovato come intersezione in caso corrisponda ad un punto del poligono
                    if temp == self.intersections[len(self.intersections) - 1]:
                        flag=1
                if flag == 0:
                    self.intersections.append(temp)

            flag=0



        counter = 0
        for j in range(len(self.segment)): # implemento un algoritmo di ray-casting point in polygon
            origin[0] = self.segment[j].x
            origin[1] = self.segment[j].y
            end[0] = 1000000 #sarebbe un valore grandissimo che me la renda una semiretta
            end[1] = self.segment[j].y

            val.SetFirstSegment(origin, end) # setto il primo segmento orizzontale passante per segment[j]

            for i in range(len(self.points)):
                origin[0] = self.points[i].x
                origin[1] = self.points[i].y
                end[0] = self.points[(i+1) % len(self.points)].x
                end[1] = self.points[(i+1) % len(self.points)].y

                val.SetSecondSegment(origin, end) # origin e end sono due vertici consecutivi del poligono(lato)
                if val.ComputeIntersection() == True and val.PointOfIntersection(origin, end).x > self.segment[j].x: #verifico quante intersezioni ci sono tra i due segmenti, considerando solo quelle di ascissa > segment[j]
                    counter = counter+1

            if counter % 2 != 0 : # se ho un numero dispari di intersezione, il punto è all'interno del poligono quindi lo aggiungo a intersections
                self.intersections.append(self.segment[j])
            counter = 0

        self.intersections.sort()
        return self.intersections

    def NewPoints(self):  # rida newPoints, NB i punti del poligono erano già stati aggiunti in CreatePolygon#
        for i in range(len(self.intersections)):
            found = 0
            for j in range(len(self.points)):   # verifico che un punto di intersezione non sia un vertice che esiste già
                if self.intersections[i] == self.points[j] :
                    found = 1

            if found == 0 :
                self.newPoints.append(self.intersections[i]) # lo aggiungo ai nuovi punti

        return self.newPoints

    def OrderOnPolygon(self):  #mette in ordine (antiorario) perimeterPoints
        flag = 0
        for i in range (len(self.points)):
            if len(self.perimeterPoints) > 0: #verifico che il punto non sia già stato aggiunto (nel caso un'intersezione coincida con un vertice)
                if self.points[i] == self.perimeterPoints[len(self.perimeterPoints) - 1] :
                    flag = 1
            if flag == 0 :
                self.perimeterPoints.append(self.points[i])
            flag=0
            for j in range (len(self.intersections)):
                if self.PointOnLine(self.points[i], self.points[(i+1) % (len(self.points))], self.intersections[j]) == True :  #verifico che l'intersezione appartenga al lato
                    if len(self.perimeterPoints)>0 :  #verifico che il punto non sia già stato aggiunto (nel caso un'intersezione coincida con un vertice)
                        if self.intersections[j] == self.perimeterPoints[len(self.perimeterPoints)-1] :
                            flag=1
                    if flag == 0:
                        self.perimeterPoints.append(self.intersections[j])
            flag=0
        return self.perimeterPoints

    def CrossProduct(self,p1: Point): #ritorna vero se p1 è a destra del segmento
        right = False
        edges= [[0,0],[0,0]]
        edges[0][0] = self.segment[1].x - self.segment[0].x
        edges[1][0]= self.segment[1].y - self.segment[0].y
        edges[0][1]= p1.x - self.segment[0].x
        edges[1][1]= p1.y - self.segment[0].y

        if np.linalg.det(edges) < 0 :
            right = True # arrivo da destra
        else:
            right = False # arrivo da sinistra

        return right

    def OrderVerticesOnPolygon(self):  #mette in ordine (antiorario) perimeterVertices
        flag = 0
        count = 0
        temp = 0
        numVert = len(self.points) # parto da points.size() a numerare le intersezioni perchè i vertici del poligono sono già numerati

       # numero i vertici delle intersezioni
        for i in range(len(self.intersections)) :
            numVert = numVert+i
            for j in range (len(self.points)): #controllo se l'intersezione coincide con un vertice
                if self.intersections[i] == self.points[j] :
                    flag=1
                    temp = j
            if flag == 1:
                self.intersectionVertices.append(self.polygonVertices[temp]) # assegno all'intersezione lo stesso vertice del punto a cui è uguale
                numVert = numVert - 1
            else:
                self.intersectionVertices.append(numVert)

            numVert = numVert - i
            flag=0

      # riempio il vettore con i vertici ordinati sul perimetro
        for i in range(len(self.perimeterPoints)):
            temp=0
            flag=0
            for j in range(len(self.intersections)): # controllo se il punto è un'intersezione, se lo è ne salvo la position
                if self.perimeterPoints[i] == self.intersections[j] :
                    count=1
                    temp=j
            k=0
            while k < len(self.points) and count != 1: # se il punto non è un'intersezione, ne trovo la position in points
                if self.perimeterPoints[i] == self.points[k] :
                    temp=k
                k=k+1
            if len(self.perimeterVertices) > 0 : # verifico che il vertice non sia già stato aggiunto
                if count == 1 :
                    if self.intersectionVertices[temp] == self.perimeterVertices[len(self.perimeterVertices)-1] :
                        flag=1
                else:
                    if self.polygonVertices[temp] == self.perimeterVertices[len(self.perimeterVertices)-1] :
                        flag=1

            if flag == 0 : # se non è ancora stato aggiunto, lo aggiungo
                if count == 1:
                    self.perimeterVertices.append(self.intersectionVertices[temp])
                else:
                    self.perimeterVertices.append(self.polygonVertices[temp])

            count = 0

        return self.perimeterVertices


    def CutPolygon(self): #ritorna i poligoni tagliati
        position = 0 # vertice da cui inizia un poligono
        polygon= [] # vettore d'appoggio in cui salvo il poligono tagliato
        copy = []
        stop = False
        check = 0
        newVertex = True
        i=0
        while i<len(self.perimeterPoints) and stop!=True: # in range(len(self.perimeterPoints) and stop!=True):
            position = i
            while ((i + 1) % len(self.perimeterPoints) != position):  #aggiungo i vertici al nuovo poligono finchè non torno al punto di partenza
                polygon.append(self.perimeterVertices[i])
                for j in range(len(self.intersections)):
                    if self.perimeterPoints[i] == self.intersections[j]: # verifico se il punto è un'intersezione
                        if self.CrossProduct(self.perimeterPoints[i-1]) == True : # controllo se il punto precedente è a destra del segmento
                            onPerimeter = False
                            k=j+1
                            while k<len(self.intersections) and onPerimeter == False: # in range(j+1,len(self.intersections) and onPerimeter == False,1) : #percorro il vettore intersections in ordine finchè non torno sul perimetro (a scendere)
                                polygon.append(self.intersectionVertices[k])
                                for l in range(len(self.perimeterPoints)):
                                    if self.perimeterPoints[l] == self.intersections[k] :
                                        onPerimeter=True
                                        i=l # pongo i=l per riprendere il ciclo a partire dalla posizione d'uscita

                                    if onPerimeter == True :
                                        if self.perimeterPoints[position] == self.perimeterPoints[(i) % len(self.perimeterPoints)] :  #verifico se sono tornato al punto di partenza
                                            check=1
                                            break
                                        if check == 0 and self.CrossProduct(self.perimeterPoints[(i + 1) % len(self.perimeterPoints)]) == False : #altrimenti verifico se il punto successivo a quello di uscita è a sx del segmento, se lo è continuo su intersections
                                            onPerimeter = False
                                        check = 0
                                k=k+1

                        else : #controllo se il punto precedente è a sinistra del segmento
                            onPerimeter = False
                            k=j-1
                            while k>=0 and onPerimeter== False: #for k in range(j-1,0 and onPerimeter == False, -1): #percorro il vettore intersections in ordine inverso finchè non torno sul perimetro (a salire)
                                polygon.append(self.intersectionVertices[k])
                                for l in range(len(self.perimeterPoints)):
                                    if self.perimeterPoints[l] == self.intersections[k] :
                                        onPerimeter=True
                                        i=l # pongo i=l per riprendere il ciclo a partire dalla posizione d'uscita
                                    if onPerimeter == True:
                                        if self.perimeterPoints[position] == self.perimeterPoints[(i) % len(self.perimeterPoints)] :# verifico se sono tornato al punto di partenza

                                            check=1

                                        if check == 0 and self.CrossProduct(self.perimeterPoints[(i+1) % len(self.perimeterPoints)]) == True: # altrimenti verifico se il punto successivo a quello di uscita è a dx del segmento, se lo è continuo su intersections
                                            onPerimeter = False

                                        check=0
                                k=k-1
                        break

                i = i+1
                if self.perimeterPoints[position] == self.perimeterPoints[i % len(self.perimeterPoints)] : # controllo se il vertice successivo è quello di partenza, se lo è torno indietro di uno in modo da uscire con la condizione del while
                    i = i - 1
                else: #//se è il penultimo vertice del poligono, aggiungo anche l'ultimo vertice perche poi esco dal ciclo
                    if (i + 1) % len(self.perimeterPoints) == position :
                        polygon.append(self.perimeterVertices[i])

            self.cuttedPolygons.append(polygon) # ho tagliato un nuovo poligono quindi lo aggiungo a cuttedPolygons
            for r in range(len(polygon)) : # copio ogni vertice dei poligoni già tagliati in un vettore d'appoggio
                    copy.append(polygon[r])
            for s in range(len(self.perimeterVertices)):  #controllo se lo s-esimo vertice non è stato ancora aggiunto
                    newVertex = True
                    for t in range(len(copy)):
                        if self.perimeterVertices[s] == copy[t] :
                            newVertex = False
                    if newVertex == True :
                        i = s - 1
                        break

            if newVertex == False: #//se non trovo un nuovo vertice vuol dire che ho finito tutti i vertici
                stop=True
            polygon=[]
            position=0
            i=i+1

        return self.cuttedPolygons





