from unittest import TestCase

import Project as PolCut

tol = 1.0E-7

class Intersector1D1D(TestCase):
    def test_TestSegmentIntersection(self):
        a=[1,0]
        b=[5,0]
        c=[3,-6]
        d=[3,6]
        intersector = PolCut.Intersector1D1D()
        intersector.SetFirstSegment(a,b)
        intersector.SetSecondSegment(c,d)
        try:
            self.assertTrue(intersector.ComputeIntersection())
        except Exception as ex:
            self.fail()

        try:
            self.assertEqual(intersector.PointOfIntersection(c,d).x, 3.0)
            self.assertEqual(intersector.PointOfIntersection(c, d).y, 0.0)
        except Exception as ex:
            self.fail()


    def test_TestOnLineIntersection(self):
        a=[3,6]
        b=[3,2]
        c=[5,0]
        d=[1,0]
        intersector = PolCut.Intersector1D1D()
        intersector.SetFirstSegment(a,b)
        intersector.SetSecondSegment(c,d)
        try:
            self.assertTrue(intersector.ComputeIntersection())
        except Exception as ex:
            self.fail()

        try:
            self.assertEqual(intersector.PointOfIntersection(c,d).x, 3.0)
            self.assertEqual(intersector.PointOfIntersection(c, d).y, 0.0)
        except Exception as ex:
            self.fail()

