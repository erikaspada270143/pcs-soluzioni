from unittest import TestCase

import Project as PolCut

tol = 1.0E-7

class ConvexPolygons(TestCase):

    def test_TestTriangle(self): #TEST FIGURA CONVESSA A SCELTA TRIANGOLO
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        triangle = [[-3.0, 0.0], [3.0, 0.0], [0.0, 5.0]]
        vertices = [0, 1, 2]
        segment = [[0.0, 1.0], [0.0, 6.0]]
        polygon.CreatePolygon(triangle, vertices, segment)
        intersections = [[0.0, 5.0], [0.0, 1.0], [0.0, 0.0]]
        newpoints = [[-3.0, 0.0], [3.0, 0.0], [0.0, 5.0], [0.0, 1.0],[0.0,0.0]]
        intersectionsTrue= polygon.Intersection()
        newpointsTrue= polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(-3.0,0.0), PolCut.Point(3.0,0.0), intersectionsTrue[2]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(-3.0,0.0), PolCut.Point(3.0,0.0), intersectionsTrue[0]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE  {-3.0,0.0},{0.0,0.0},{3.0,0.0},{0.0,5.0}
        perimeterPoints = [[-3.0, 0.0], [0.0, 0.0], [3.0, 0.0], [0.0, 5.0]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0]- perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1]- perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE PV FUNZIONA
        try:
            self.assertTrue(polygon.CrossProduct(PolCut.Point(3.0,0.0)))
            self.assertFalse(polygon.CrossProduct(PolCut.Point(-3.0,0.0)))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0, 4, 1, 2]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0,4,3,2], [1,2,3,4]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()




    def test_TestRectangle(self): #RETTANGOLO
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        rectangle = [[1.0, 1.0], [5.0, 1.0], [5.0, 3.1], [1.0, 3.1]]
        vertices = [0, 1, 2, 3]
        segment = [[2.0, 1.2], [4.0, 3.0]]
        polygon.CreatePolygon(rectangle, vertices, segment)
        intersections = [[4.11111111111, 3.1], [4.0, 3.0], [2.0, 1.2], [1.77777777778, 1.0]]
        newpoints = [[1.0, 1.0], [5.0, 1.0], [5.0, 3.1], [1.0, 3.1], [4.11111111111, 3.1], [4.0, 3.0], [2.0, 1.2],[1.77777777778, 1.0]]
        intersectionsTrue= polygon.Intersection()
        newpointsTrue= polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(1.0, 1.0), PolCut.Point(5.0, 1.0), intersectionsTrue[3]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(1.0, 1.0), PolCut.Point(5.0, 1.0), intersectionsTrue[0]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
        perimeterPoints = [[1.0, 1.0], [1.77777777778, 1.0], [5.0, 1.0], [5.0, 3.1], [4.11111111111, 3.1], [1.0, 3.1]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0]- perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1]- perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE PV FUNZIONA
        try:
            self.assertTrue(polygon.CrossProduct(PolCut.Point(5.0, 1.0)))
            self.assertFalse(polygon.CrossProduct(PolCut.Point(1.0, 1.0)))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0, 7, 1, 2, 4, 3]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0, 7, 6, 5, 4, 3], [1, 2, 4, 5, 6, 7]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()




    def test_TestPentagon(self): #PENTAGONO
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        pentagon = [[2.5, 1.0], [4.0, 2.1], [3.4, 4.2], [1.6, 4.2],[1.0,2.1]]
        vertices = [0,1,2,3,4]
        segment = [[1.4, 2.75], [3.6, 2.2]]
        polygon.CreatePolygon(pentagon, vertices, segment)
        intersections = [[4.0, 2.1], [3.6, 2.2], [1.4, 2.75], [1.2, 2.8]]
        newpoints = [[2.5, 1.0], [4.0, 2.1], [3.4, 4.2], [1.6, 4.2],[1.0,2.1],[3.6, 2.2],[1.4, 2.75],[1.2, 2.8]]
        intersectionsTrue= polygon.Intersection()
        newpointsTrue= polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(1.6,4.2), PolCut.Point(1.0,2.1), intersectionsTrue[3]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(1.6,4.2), PolCut.Point(1.0,2.1), intersectionsTrue[0]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
        perimeterPoints = [[2.5, 1.0], [4.0, 2.1], [3.4, 4.2], [1.6, 4.2], [1.2, 2.8], [1.0, 2.1]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0]- perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1]- perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()


        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0,1,2,3,7,4]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0,1,5,6,7,4], [2,3,7,6,5,1]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()

