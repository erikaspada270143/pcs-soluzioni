from unittest import TestCase

import Project as PolCut

tol = 1.0E-7


class ConcavePolygons(TestCase):

    def test_TestConcave1(self):
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        concave1 = [[1.5, 1.0], [5.6, 1.5], [5.5, 4.8], [4.0, 6.2], [3.2, 4.2],[1.0,4.0]]
        vertices = [0, 1, 2, 3, 4,5]
        segment = [[2.0, 3.7], [4.1, 5.9]]
        polygon.CreatePolygon(concave1, vertices, segment)
        intersections = [[4.20432692,6.00929487], [4.1,5.9], [3.72131147,5.50327868], [2.40859728,4.12805429],[2.0,3.7],[1.19121621,2.85270270]]
        newpoints = [[1.5, 1.0], [5.6, 1.5], [5.5, 4.8], [4.0, 6.2], [3.2, 4.2],[1.0,4.0],[4.20432692,6.00929487], [4.1,5.9], [3.72131147,5.50327868], [2.40859728,4.12805429],[2.0,3.7],[1.19121621,2.85270270]]
        intersectionsTrue = polygon.Intersection()
        newpointsTrue = polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(5.5, 4.8), PolCut.Point(4, 6.2), intersectionsTrue[0]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(5.5, 4.8), PolCut.Point(4, 6.2), intersectionsTrue[3]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
        perimeterPoints = [[1.5,1.0], [5.6,1.5], [5.5,4.8], [4.20432692,6.00929487], [4.0,6.2], [3.72131147,5.50327868],[3.2,4.2],[2.40859728,4.12805429],[1.0,4.0],[1.19121621,2.85270270]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0] - perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1] - perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0,1,2,6,3,8,4,9,5,11]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0,1,2,6,7,8,4,9,10,11], [3,8,7,6],[5,11,10,9]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()




    def test_TestConcave2(self):    #TEST D'AURIA
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        concave2 = [[2.0,-2.0],[0.0,-1.0],[3.0,1.0],[0.0,2.0],[3.0,2.0],[3.0,3.0],[-1.0,3.0],[-3.0,1.0],[0.0,0.0],[-3.0,-2.0]]
        vertices = [0,1,2,3,4,5,6,7,8,9]
        segment = [[0.0,-3.0],[0.0,4.0]]
        polygon.CreatePolygon(concave2, vertices, segment)
        intersections = [[0.0,3.0],[0.0,2.0],[0.0,0.0],[0.0,-1.0],[0.0,-2.0]]
        newpoints = [[2.0, -2.0], [0.0, -1.0], [3.0, 1.0], [0.0, 2.0], [3.0, 2.0], [3.0, 3.0], [-1.0, 3.0], [-3.0, 1.0], [0.0, 0.0],[-3.0, -2.0], [0.0, 3.0], [0.0, -2.0]]
        intersectionsTrue = polygon.Intersection()
        newpointsTrue = polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(3.0,3.0), PolCut.Point(-1.0,3.0), intersectionsTrue[0]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(3.0,3.0), PolCut.Point(-1.0,3.0), intersectionsTrue[3]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
        perimeterPoints = [[2.0,-2.0],[0.0,-1.0],[3.0,1.0],[0.0,2.0],[3.0,2.0],[3.0,3.0],[0.0,3.0],[-1.0,3.0],[-3.0,1.0],[0.0,0.0],[-3.0,-2.0],[0.0,-2.0]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0] - perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1] - perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0,1,2,3,4,5,10,6,7,8,9,11]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0,1,11],[2,3,8,1],[4,5,10,3],[6,7,8,3,10],[9,11,1,8]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()




    def test_TestConcave3(self):  #TEST D'AURIA
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        concave3 = [[2.0,-2.0],[0.0,-1.0],[3.0,1.0],[0.0,2.0],[3.0,2.0],[3.0,3.0],[-1.0,3.0],[-3.0,1.0],[0.0,0.0],[-3.0,-2.0]]
        vertices = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        segment = [-4.0,-4.0],[4.0,4.0]
        polygon.CreatePolygon(concave3, vertices, segment)
        intersections = [[3.0,3.0],[2.0,2.0],[1.5,1.5],[0.0,0.0],[-2.0,-2.0]]
        newpoints = [[2.0,-2.0],[0.0,-1.0],[3.0,1.0],[0.0,2.0],[3.0,2.0],[3.0,3.0],[-1.0,3.0],[-3.0,1.0],[0.0,0.0],[-3.0,-2.0],[2.0,2.0],[1.5,1.5],[-2.0,-2.0]]
        intersectionsTrue = polygon.Intersection()
        newpointsTrue = polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(3.0, 1.0), PolCut.Point(0.0, 2.0), intersectionsTrue[2]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(3.0, 1.0), PolCut.Point(0.0, 2.0), intersectionsTrue[3]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
        perimeterPoints = [[2.0,-2.0],[0.0,-1.0],[3.0,1.0],[1.5,1.5],[0.0,2.0],[2.0,2.0],[3.0,2.0],[3.0,3.0],[-1.0,3.0],[-3.0,1.0],[0.0,0.0],[-3.0,-2.0],[-2.0,-2.0]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0] - perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1] - perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0,1,2,11,3,10,4,5,6,7,8,9,12]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0,1,2,11,8,12],[3,10,5,6,7,8,11],[4,5,10],[9,12,8]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()




    def test_TestConcave4(self): #FIGURA CONCAVA A SCELTA
        polygon = PolCut.Polygon()
        intersection = PolCut.Intersector1D1D()
        concave4 = [[4.0,-2.0],[0.0,5.0],[-4.0,-2.0],[-2.0,-2.0],[-1.0,0.0],[1.0,0.0],[2.0,-2.0]]
        vertices = [0, 1, 2, 3, 4,5,6]
        segment = [[-2.02, 0.51], [1.0,-1.0]]
        polygon.CreatePolygon(concave4, vertices, segment)
        intersections = [[3.0,-2.0],[1.66666667,-1.33333333],[-1.0,0.0],[-2.02, 0.51],[-2.44444444,0.72222222]]
        newpoints = [[4.0,-2.0],[0.0,5.0],[-4.0,-2.0],[-2.0,-2.0],[-1.0,0.0],[1.0,0.0],[2.0,-2.0],[3.0,-2.0],[1.66666667,-1.33333333],[-2.02, 0.51],[-2.44444444,0.72222222]]
        intersectionsTrue = polygon.Intersection()
        newpointsTrue = polygon.NewPoints()

        # TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
        try:
            for i in range(len(intersectionsTrue)):
                self.assertTrue(abs(intersectionsTrue[i].x - intersections[i][0]) < tol)
                self.assertTrue(abs(intersectionsTrue[i].y - intersections[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
        try:
            for i in range(len(newpoints)):
                self.assertTrue(abs(newpointsTrue[i].x - newpoints[i][0]) < tol)
                self.assertTrue(abs(newpointsTrue[i].y - newpoints[i][1]) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
        try:
            self.assertTrue(polygon.PointOnLine(PolCut.Point(2.0,-2.0), PolCut.Point(4.0,-2.0), intersectionsTrue[0]))
            self.assertFalse(polygon.PointOnLine(PolCut.Point(2.0,-2.0), PolCut.Point(4.0,-2.0), intersectionsTrue[3]))
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
        perimeterPoints = [[4.0,-2.0],[0.0,5.0],[-2.44444444,0.72222222],[-4.0,-2.0],[-2.0,-2.0],[-1.0,0.0],[1.0,0.0],[1.66666667,-1.33333333],[2.0,-2.0],[3.0,-2.0]]
        perimeterPointsTrue = polygon.OrderOnPolygon()
        try:
            for i in range(len(perimeterPoints)):
                self.assertTrue(abs(perimeterPoints[i][0] - perimeterPointsTrue[i].x) < tol)
                self.assertTrue(abs(perimeterPoints[i][1] - perimeterPointsTrue[i].y) < tol)
        except Exception as ex:
            self.fail()

        # TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
        perimeterVertices = [0,1,10,2,3,4,5,8,6,7]
        try:
            self.assertEqual(polygon.OrderVerticesOnPolygon(), perimeterVertices)
        except Exception as ex:
            self.fail()

        # TEST PER TAGLIO POLIGONO
        cuttedPolygons = [[0,1,10,9,4,5,8,7], [2,3,4,9,10],[6,7,8]]
        try:
            self.assertEqual(polygon.CutPolygon(), cuttedPolygons)
        except Exception as ex:
            self.fail()
