#include "test_ConcavePolygons.hpp"
#include "test_ConvexPolygons.hpp"
#include "test_Intersector1D1D.hpp"

#include <gtest/gtest.h>

int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
