#ifndef __TEST_INTERSECTOR1D1D_H
#define __TEST_INTERSECTOR1D1D_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Project.hpp"

using namespace ProjectNamespace;
using namespace testing;
using namespace std;

namespace Intersector1D1DTesting {

TEST(TestIntersector1D1D, TestSegmentIntersection)
{
  Vector2d a(1, 0);
  Vector2d b(5, 0);
  Vector2d c(3, -6);
  Vector2d d(3, 6);
  Intersector1D1D intersector;

  intersector.SetFirstSegment(a, b);
  intersector.SetSecondSegment(c, d);
  EXPECT_TRUE(intersector.ComputeIntersection());
  EXPECT_EQ(intersector.PointOfIntersection(c,d)._x, 3.0);
  EXPECT_EQ(intersector.PointOfIntersection(c,d)._y, 0.0);
}

TEST(TestIntersector1D1D, TestOnLineIntersection)
{
  Vector2d a(3, 6);
  Vector2d b(3, 2);
  Vector2d c(5, 0);
  Vector2d d(1, 0);

  Intersector1D1D intersector;

  intersector.SetFirstSegment(a, b);
  intersector.SetSecondSegment(c, d);
  EXPECT_TRUE(intersector.ComputeIntersection());
  EXPECT_EQ(intersector.PointOfIntersection(c,d),Point(3.0,0.0));
}

}
#endif




