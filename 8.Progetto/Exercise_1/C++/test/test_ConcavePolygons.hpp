#ifndef __TEST_CONCAVEPOLYGONS_H
#define __TEST_CONCAVEPOLYGONS_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Project.hpp"

using namespace ProjectNamespace;
using namespace testing;
using namespace std;


namespace ConcavePolygonTesting {

    TEST(TestPolygon,TestConcave1)
  {
      Polygon polygon;
      Intersector1D1D intersection;
      const vector<vector<double>> concave1{{1.5,1.0},{5.6,1.5},{5.5,4.8},{4.0,6.2},{3.2,4.2},{1.0,4.0}};
      const vector<int> vertices{0,1,2,3,4,5};
      const vector<vector<double>> segment {{2.0,3.7},{4.1,5.9}};
      polygon.CreatePolygon(concave1,vertices,segment);
      const vector<Point> intersections{{4.20432692,6.00929487},{4.1,5.9},{3.72131147,5.50327868},{2.40859728,4.12805429},{2.0,3.7},{1.19121621,2.85270270}};
      const vector<Point> newpoints{{1.5,1.0},{5.6,1.5},{5.5,4.8},{4.0,6.2},{3.2,4.2},{1.0,4.0},{4.20432692,6.00929487},{4.1,5.9},{3.72131147,5.50327868},{2.40859728,4.12805429},{2.0,3.7},{1.19121621,2.85270270}};
      const vector<Point> intersectionsTrue=polygon.Intersection();
      const vector<Point> newpointsTrue=polygon.NewPoints();


      //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
      for(unsigned int i =0; i<intersections.size(); i++)
      {
          EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
          EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
      }

     //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
      for(unsigned int i =0; i<newpoints.size(); i++)
      {
          EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
          EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
      }

       //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
      EXPECT_TRUE(polygon.PointOnLine(Point{5.5, 4.8},Point{4, 6.2},intersectionsTrue[0]));
      EXPECT_FALSE(polygon.PointOnLine(Point{5.5, 4.8},Point{4, 6.2},intersectionsTrue[3]));

      //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
      const vector<Point> perimeterPoints{{1.5,1.0},{5.6,1.5},{5.5,4.8},{4.20432692,6.00929487},{4.0,6.2},{3.72131147,5.50327868},{3.2,4.2},{2.40859728,4.12805429},{1.0,4.0},{1.19121621,2.85270270}};
      const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
           for(unsigned int i =0; i<perimeterPoints.size(); i++)
           {
             EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
             EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
           }

     //TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
     const vector<int> perimeterVertices {0,1,2,6,3,8,4,9,5,11};
     EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

     //TEST PER TAGLIO polygon
     const vector<vector<int>> cuttedPolygons{{0,1,2,6,7,8,4,9,10,11},{3,8,7,6},{5,11,10,9}};
     EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);

  }

    TEST(TestPolygon,TestConcave2)  //TEST D'AURIA
  {
      Polygon polygon;
      Intersector1D1D intersection;
      const vector<vector<double>> concave2{{2.0,-2.0},{0.0,-1.0},{3.0,1.0},{0.0,2.0},{3.0,2.0},{3.0,3.0},{-1.0,3.0},{-3.0,1.0},{0.0,0.0},{-3.0,-2.0}};
      const vector<int> vertices{0,1,2,3,4,5,6,7,8,9};
      const vector<vector<double>> segment {{0.0,-3.0},{0.0,4.0}};
      polygon.CreatePolygon(concave2,vertices,segment);
      const vector<Point> intersections{{0.0,3.0},{0.0,2.0},{0.0,0.0},{0.0,-1.0},{0.0,-2.0}};
      const vector<Point> newpoints{{2.0,-2.0},{0.0,-1.0},{3.0,1.0},{0.0,2.0},{3.0,2.0},{3.0,3.0},{-1.0,3.0},{-3.0,1.0},{0.0,0.0},{-3.0,-2.0},{0.0,3.0},{0.0,-2.0}};
      const vector<Point> intersectionsTrue=polygon.Intersection();
      const vector<Point> newpointsTrue=polygon.NewPoints();


      //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
      for(unsigned int i =0; i<intersections.size(); i++)
      {
          EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
          EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
      }

     //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
      for(unsigned int i =0; i<newpoints.size(); i++)
      {
          EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
          EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
      }

      //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
      EXPECT_TRUE(polygon.PointOnLine(Point{3.0,3.0},Point{-1.0,3.0},intersectionsTrue[0]));
      EXPECT_FALSE(polygon.PointOnLine(Point{3.0,3.0},Point{-1.0,3.0},intersectionsTrue[3]));

      //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
      const vector<Point> perimeterPoints{{2.0,-2.0},{0.0,-1.0},{3.0,1.0},{0.0,2.0},{3.0,2.0},{3.0,3.0},{0.0,3.0},{-1.0,3.0},{-3.0,1.0},{0.0,0.0},{-3.0,-2.0},{0.0,-2.0}};
      const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
           for(unsigned int i =0; i<perimeterPoints.size(); i++)
           {
             EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
             EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
           }

     //TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
     const vector<int> perimeterVertices {0,1,2,3,4,5,10,6,7,8,9,11};
     EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

     //TEST PER TAGLIO polygon
     const vector<vector<int>> cuttedPolygons{{0,1,11},{2,3,8,1},{4,5,10,3},{6,7,8,3,10},{9,11,1,8}};
     EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);

  }
 TEST(TestPolygon,TestConcave3)  //TEST D'AURIA
{
    Polygon polygon;
    Intersector1D1D intersection;
    const vector<vector<double>> concave3{{2.0,-2.0},{0.0,-1.0},{3.0,1.0},{0.0,2.0},{3.0,2.0},{3.0,3.0},{-1.0,3.0},{-3.0,1.0},{0.0,0.0},{-3.0,-2.0}};
    const vector<int> vertices{0,1,2,3,4,5,6,7,8,9};
    const vector<vector<double>> segment {{-4.0,-4.0},{4.0,4.0}};
    polygon.CreatePolygon(concave3,vertices,segment);
    const vector<Point> intersections{{3.0,3.0},{2.0,2.0},{1.5,1.5},{0.0,0.0},{-2.0,-2.0}};
    const vector<Point> newpoints{{2.0,-2.0},{0.0,-1.0},{3.0,1.0},{0.0,2.0},{3.0,2.0},{3.0,3.0},{-1.0,3.0},{-3.0,1.0},{0.0,0.0},{-3.0,-2.0},{2.0,2.0},{1.5,1.5},{-2.0,-2.0}};
    const vector<Point> intersectionsTrue=polygon.Intersection();
    const vector<Point> newpointsTrue=polygon.NewPoints();


    //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
    for(unsigned int i =0; i<intersections.size(); i++)
    {
        EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
        EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
    }

   //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
    for(unsigned int i =0; i<newpoints.size(); i++)
    {
        EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
        EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
    }

    //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
    EXPECT_TRUE(polygon.PointOnLine(Point{3.0,1.0},Point{0.0,2.0},intersectionsTrue[2]));
    EXPECT_FALSE(polygon.PointOnLine(Point{3.0,1.0},Point{0.0,2.0},intersectionsTrue[3]));

    //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
    const vector<Point> perimeterPoints{{2.0,-2.0},{0.0,-1.0},{3.0,1.0},{1.5,1.5},{0.0,2.0},{2.0,2.0},{3.0,2.0},{3.0,3.0},{-1.0,3.0},{-3.0,1.0},{0.0,0.0},{-3.0,-2.0},{-2.0,-2.0}};
    const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
         for(unsigned int i =0; i<perimeterPoints.size(); i++)
         {
           EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
           EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
         }

   //TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
   const vector<int> perimeterVertices {0,1,2,11,3,10,4,5,6,7,8,9,12};
   EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

   //TEST PER TAGLIO polygon
   const vector<vector<int>> cuttedPolygons{{0,1,2,11,8,12},{3,10,5,6,7,8,11},{4,5,10},{9,12,8}};
   EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);

}
 }



TEST(TestPolygon,TestConcave4)
{
  Polygon polygon;
  Intersector1D1D intersection;
  const vector<vector<double>> concave4{{4.0,-2.0},{0.0,5.0},{-4.0,-2.0},{-2.0,-2.0},{-1.0,0.0},{1.0,0.0},{2.0,-2.0}};
  const vector<int> vertices{0,1,2,3,4,5,6};
  const vector<vector<double>> segment {{-2.02, 0.51},{1.0,-1.0}};
  polygon.CreatePolygon(concave4,vertices,segment);
  const vector<Point> intersections{{3.0,-2.0},{1.66666667,-1.33333333},{-1.0,0.0},{-2.02, 0.51},{-2.44444444,0.72222222}};
  const vector<Point> newpoints{{4.0,-2.0},{0.0,5.0},{-4.0,-2.0},{-2.0,-2.0},{-1.0,0.0},{1.0,0.0},{2.0,-2.0},{3.0,-2.0},{1.66666667,-1.33333333},{-2.02, 0.51},{-2.44444444,0.72222222}};
  const vector<Point> intersectionsTrue=polygon.Intersection();
  const vector<Point> newpointsTrue=polygon.NewPoints();


  //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
  for(unsigned int i =0; i<intersections.size(); i++)
  {
      EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
      EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
  }

 //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
  for(unsigned int i =0; i<newpoints.size(); i++)
  {
      EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
      EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
  }

   //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
  EXPECT_TRUE(polygon.PointOnLine(Point{2.0,-2.0},Point{4.0,-2.0},intersectionsTrue[0]));
  EXPECT_FALSE(polygon.PointOnLine(Point{2.0,-2.0},Point{4.0,-2.0},intersectionsTrue[3]));

  //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
  const vector<Point> perimeterPoints{{4.0,-2.0},{0.0,5.0},{-2.44444444,0.72222222},{-4.0,-2.0},{-2.0,-2.0},{-1.0,0.0},{1.0,0.0},{1.66666667,-1.33333333},{2.0,-2.0},{3.0,-2.0}};
  const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
       for(unsigned int i =0; i<perimeterPoints.size(); i++)
       {
         EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
         EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
       }

 //TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
 const vector<int> perimeterVertices {0,1,10,2,3,4,5,8,6,7};
 EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

 //TEST PER TAGLIO polygon
 const vector<vector<int>> cuttedPolygons{{0,1,10,9,4,5,8,7},{2,3,4,9,10},{6,7,8}};
 EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);


}
#endif




