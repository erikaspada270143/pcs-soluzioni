#ifndef __TEST_CONVEXPOLYGON_H
#define __TEST_CONVEXPOLYGON_H

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <gmock/gmock-matchers.h>

#include "Eigen"

#include "Project.hpp"

using namespace ProjectNamespace;
using namespace testing;
using namespace std;


namespace ConvexPolygonTesting {

TEST(TestPolygon, TestTriangle)
{
 Polygon polygon;
 Intersector1D1D intersection;
 const vector<vector<double>> triangle{{-3.0,0.0},{3.0,0.0},{0.0,5.0}};
 const vector<int> vertices{0,1,2};
 const vector<vector<double>> segment {{0.0,1.0},{0.0,6.0}};
 polygon.CreatePolygon(triangle,vertices,segment);
 const vector<Point> intersections{{0.0,5.0},{0.0,1.0},{0.0,0.0}};
 const vector<Point> newpoints{{-3.0,0.0},{3.0,0.0},{0.0,5.0},{0.0,1.0},{0.0,0.0}};
 const vector<Point> intersectionsTrue=polygon.Intersection();
 const vector<Point> newpointsTrue=polygon.NewPoints();


 //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
 for(unsigned int i =0; i<intersections.size(); i++)
 {
   EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
   EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
 }

 //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
 for(unsigned int i =0; i<newpoints.size(); i++)
 {
   EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
   EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
 }

 //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
 EXPECT_TRUE(polygon.PointOnLine(Point{-3.0,0.0},Point{3.0,0.0},intersectionsTrue[2]));
 EXPECT_FALSE(polygon.PointOnLine(Point{-3.0,0.0},Point{3.0,0.0},intersectionsTrue[0]));


 //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
 const vector<Point> perimeterPoints{{-3.0,0.0},{0.0,0.0},{3.0,0.0},{0.0,5.0}};
 const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
      for(unsigned int i =0; i<perimeterPoints.size(); i++)
      {
        EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
        EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
      }
//TEST PER VEDERE CHE PV FUNZIONA

EXPECT_FALSE(polygon.CrossProduct(Point{-3.0,0.0}));
EXPECT_TRUE(polygon.CrossProduct(Point{3.0,0.0}));


//TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
const vector<int> perimeterVertices {0,4,1,2};
EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

//TEST PER TAGLIO polygon
const vector<vector<int>> cuttedPolygons{{0,4,3,2},{1,2,3,4}};
EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);
}


   TEST(TestPolygon, TestRectangle)
 {
     Polygon polygon;
     Intersector1D1D intersection;
     const vector<vector<double>> rectangle{{1.0,1.0},{5.0,1.0},{5.0,3.1},{1.0,3.1}};
     const vector<int> vertices{0,1,2,3};
     const vector<vector<double>> segment {{2.0,1.2},{4.0,3.0}};
     polygon.CreatePolygon(rectangle,vertices,segment);
     const vector<Point> intersections{{4.11111111111,3.1},{4.0,3.0},{2.0,1.2},{1.77777777778,1.0}};
     const vector<Point> newpoints{{1.0,1.0},{5.0,1.0},{5.0,3.1},{1.0,3.1},{4.11111111111,3.1},{4.0,3.0},{2.0,1.2},{1.77777777778,1.0}};
     const vector<Point> intersectionsTrue=polygon.Intersection();
     const vector<Point> newpointsTrue=polygon.NewPoints();

     //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
     for(unsigned int i =0; i<intersections.size(); i++)
     {
       EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
       EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
     }

      //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
     for(unsigned int i =0; i<newpoints.size(); i++)
     {
       EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
       EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
     }

     //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
     EXPECT_TRUE(polygon.PointOnLine(Point{1.0,1.0},Point{5.0,1.0},intersectionsTrue[3]));
     EXPECT_FALSE(polygon.PointOnLine(Point{1.0,1.0},Point{5.0,1.0},intersectionsTrue[0]));
     EXPECT_TRUE(polygon.PointOnLine(Point{5.0,3.1},Point{1.0,3.1},intersectionsTrue[0]));
     EXPECT_FALSE(polygon.PointOnLine(Point{5.0,3.1},Point{1.0,3.1},intersectionsTrue[3]));

     //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
     const vector<Point> perimeterPoints{{1.0,1.0},{1.77777777778,1.0},{5.0,1.0},{5.0,3.1},{4.11111111111,3.1},{1.0,3.1}};
     const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
          for(unsigned int i =0; i<perimeterPoints.size(); i++)
          {
            EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
            EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
          }
    //TEST PER VEDERE CHE PV FUNZIONA

    EXPECT_FALSE(polygon.CrossProduct(Point{1.0,1.0}));
    EXPECT_TRUE(polygon.CrossProduct(Point{5.0,1.0}));
    EXPECT_TRUE(polygon.CrossProduct(Point{5.0,3.1}));
    EXPECT_FALSE(polygon.CrossProduct(Point{1.0,3.1}));

    //TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
    const vector<int> perimeterVertices {0,7,1,2,4,3};
    EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

    //TEST PER TAGLIO polygon
    const vector<vector<int>> cuttedPolygons{{0,7,6,5,4,3},{1,2,4,5,6,7}};
    EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);


  }
    TEST(TestPolygon,TestPentagon)
  {
      Polygon polygon;
      Intersector1D1D intersection;
      const vector<vector<double>> pentagon{{2.5,1.0},{4.0,2.1},{3.4,4.2},{1.6,4.2},{1.0,2.1}};
      const vector<int> vertices{0,1,2,3,4};
      const vector<vector<double>> segment {{1.4,2.75},{3.6,2.2}};
      polygon.CreatePolygon(pentagon,vertices,segment);
      const vector<Point> intersections{{4.0,2.1},{3.6,2.2},{1.4,2.75},{1.2,2.8}};
      const vector<Point> newpoints{{2.5,1.0},{4.0,2.1},{3.4,4.2},{1.6,4.2},{1.0,2.1},{3.6,2.2},{1.4,2.75},{1.2,2.8}};
      const vector<Point> intersectionsTrue=polygon.Intersection();
      const vector<Point> newpointsTrue=polygon.NewPoints();


      //TEST PER VEDERE CHE TROVA LE INTERSEZIONI GIUSTE
      for(unsigned int i =0; i<intersections.size(); i++)
      {
          EXPECT_TRUE(abs(intersectionsTrue[i]._x-intersections[i]._x) < 1.0E-7 );
          EXPECT_TRUE(abs(intersectionsTrue[i]._y-intersections[i]._y) < 1.0E-7 );
      }

      //TEST PER VEDERE CHE INSERISCE BENE I PUNTI IN NEWPOINTS
      for(unsigned int i =0; i<newpoints.size(); i++)
      {
          EXPECT_TRUE(abs(newpointsTrue[i]._x-newpoints[i]._x) < 1.0E-7 );
          EXPECT_TRUE(abs(newpointsTrue[i]._y-newpoints[i]._y) < 1.0E-7 );
      }

     //TEST PER VEDERE CHE IL PUNTO APPARTENGA AL LATO
      EXPECT_TRUE(polygon.PointOnLine(Point{1.6,4.2},Point{1.0,2.1},intersectionsTrue[3]));
      EXPECT_FALSE(polygon.PointOnLine(Point{1.6,4.2},Point{1.0,2.1},intersectionsTrue[0]));

      //TEST PER VEDERE CHE METTE I PUNTI IN ORDINE
      const vector<Point> perimeterPoints{{2.5,1.0},{4.0,2.1},{3.4,4.2},{1.6,4.2},{1.2,2.8},{1.0,2.1}};
      const vector<Point> perimeterPointsTrue = polygon.OrderOnPolygon();
           for(unsigned int i =0; i<perimeterPoints.size(); i++)
           {
             EXPECT_TRUE(abs(perimeterPoints[i]._x-perimeterPointsTrue[i]._x) < 1.0E-7 );
             EXPECT_TRUE(abs(perimeterPoints[i]._y-perimeterPointsTrue[i]._y) < 1.0E-7 );
           }

     //TEST PER VEDERE CHE I VERTICI SONO MESSI CORRETTAMENTE
     const vector<int> perimeterVertices {0,1,2,3,7,4};
     EXPECT_EQ(polygon.OrderVerticesOnPolygon(),perimeterVertices);

     //TEST PER TAGLIO polygon
     const vector<vector<int>> cuttedPolygons{{0,1,5,6,7,4},{2,3,7,6,5,1}};
     EXPECT_EQ(polygon.CutPolygon(),cuttedPolygons);

  }

}
#endif




