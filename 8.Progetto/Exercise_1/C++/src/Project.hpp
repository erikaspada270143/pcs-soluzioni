#ifndef EMPTYCLASS_H
#define EMPTYCLASS_H
#include <iostream>
#include <Eigen>
#include <algorithm>
using namespace std;
using namespace Eigen;

namespace ProjectNamespace {

  class Point {
    public:
      double _x;
      double _y;

      Point() {_x = 0.0, _y = 0.0; } //costruttore default che mette a zero
      Point(const double& x,
            const double& y) { _x = x , _y = y; } // costruttore
      bool operator == (const Point& point) const {return _x == point._x && _y == point._y;}
      bool operator < (const Point& point) const {if(_x!=point._x){return _x > point._x;}
                                                            else{return _y > point._y;};}
   };


    class Intersector1D1D : public Point
  {

    public:
        double tolerance;

        Vector2d resultParametricCoordinates; // s segmenti (posizione 0: segmento, posizione 1: lato)
        Point originFirstSegment; //x0,y0
        Vector2d rightHandSide;  //in posizione 0 x_1 - x_0, in pos 1 y_1 - y_0
        Matrix2d matrixTangentVector; //t segmenti (direzioni)
        Point intersersectionPoint; //punto di intersezione segmenti

    public:
        Intersector1D1D();
        ~Intersector1D1D();

        void SetTolerance(const double& _tolerance) { tolerance = _tolerance; }
        void SetFirstSegment(const Vector2d& origin, const Vector2d& end);
        void SetSecondSegment(const Vector2d& origin, const Vector2d& end);
        bool ComputeIntersection(); //verifica se c'è intersezione tra il primo segmento (prolungato in retta) e il secondo segmento
        const Point& PointOfIntersection(const Vector2d& origin,const Vector2d& end);

   };

   class Polygon : Intersector1D1D
  {
    public:
      vector<Point> segment;  //punti segmento
      //tutto in senso antiorario
      vector<Point> points;  //punti poligono
      vector<int> polygonVertices;  //vertici poligono
      vector<Point> intersections;  //punti di intersezione + punti segmento se interni al poligono
      vector<int> intersectionVertices;  //vertici di intersections
      vector<Point> perimeterPoints;  //punti sul perimetro del poligono
      vector<int> perimeterVertices;  //vertici perimeterPoints
      vector<Point> newPoints;  //tutti i punti dopo il taglio
      vector<vector<int>> cuttedPolygons;  //poligoni tagliati (vertici)

   public:
      void CreatePolygon(const vector<vector<double>>& polygonPoints, const vector<int>& vertices, const vector<vector<double>>& segmentPoints);
      bool PointOnLine(const Point& firstEdge, const Point& secondEdge, const Point& segmentEdge); //per verificare se un punto di intersezione appartiene a un lato
      vector<Point> Intersection(); //calcola le intersezioni segmento/lati poligono
      vector<Point> NewPoints(); //ridà newPoints
      vector<Point> OrderOnPolygon(); //mette in ordine (antiorario) perimeterPoints
      bool CrossProduct(const Point& p1); //ritorna vero se p1 è a destra del segmento
      vector<int> OrderVerticesOnPolygon(); //mette in ordine (antiorario) perimeterVertices
      vector<vector<int>> CutPolygon(); //ritorna i poligoni tagliati
  };
}
#endif







