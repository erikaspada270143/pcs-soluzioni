#include "Project.hpp"
#define INF 10000
namespace ProjectNamespace {


   Intersector1D1D::Intersector1D1D()
 {
  tolerance = 1.0E-7;
  resultParametricCoordinates.setZero(2);
 }
Intersector1D1D::~Intersector1D1D()
{

}

void Intersector1D1D::SetFirstSegment(const Vector2d &origin, const Vector2d &end)
{
    matrixTangentVector.col(0) = (end - origin); originFirstSegment._x= origin[0], originFirstSegment._y= origin[1];
}

void Intersector1D1D::SetSecondSegment(const Vector2d &origin, const Vector2d &end)
{
    matrixTangentVector.col(1) = (origin - end); rightHandSide[0] = origin[0] - originFirstSegment._x, rightHandSide[1] = origin[1] - originFirstSegment._y;
}

bool Intersector1D1D::ComputeIntersection()
{
    double determinant = matrixTangentVector.determinant(); //prodotto vettoriale t segmenti
    bool intersection = false;
    double check = tolerance * tolerance * matrixTangentVector.col(0).squaredNorm() *  matrixTangentVector.col(1).squaredNorm();
    if(determinant * determinant >= check) //non sono paralleli
    {
      Matrix2d solverMatrix; //inversa di MatrixTangentVector
      solverMatrix << matrixTangentVector(1,1), -matrixTangentVector(0,1), -matrixTangentVector(1,0), matrixTangentVector(0,0);
      resultParametricCoordinates = (solverMatrix * rightHandSide)/determinant; //divido per determinante matrice MatrixTangentVector, perchè calcolando l'inversa non avevo diviso per il determinante
      if (resultParametricCoordinates(1) > -tolerance  && resultParametricCoordinates(1)-1.0 < tolerance)
      {
        intersection = true;
      }
    }
    //studio i casi limite

    if(abs(resultParametricCoordinates(1)) < tolerance)
    {
      resultParametricCoordinates(1) = 0.0;  //inzio lato
    }

    else if (abs(resultParametricCoordinates(1) - 1.0) < tolerance)
    {
      resultParametricCoordinates(1) = 1.0;  //fine lato
    }

    return intersection;
}


const Point &Intersector1D1D::PointOfIntersection(const Vector2d &origin, const Vector2d &end)
{
    intersersectionPoint._x = (1 - resultParametricCoordinates(1)) * origin[0] + resultParametricCoordinates(1) * end[0];
    intersersectionPoint._y = (1 - resultParametricCoordinates(1)) * origin[1] + resultParametricCoordinates(1) * end[1];
    return intersersectionPoint;
}







 void Polygon::CreatePolygon(const vector<vector<double>>& polygonPoints, const vector<int>& vertices, const vector<vector<double>>& segmentPoints)
 {
    for (unsigned int i=0; i<vertices.size();i++)
       {
         polygonVertices.push_back(vertices[i]);
         points.push_back(Point(polygonPoints[i][0],polygonPoints[i][1]));
         newPoints.push_back(points[i]); //aggiungo gia i vertici del poligono ai miei nuovi punti
       }
   segment.push_back(Point(segmentPoints[0][0],segmentPoints[0][1]));
   segment.push_back(Point(segmentPoints[1][0],segmentPoints[1][1]));

 }

 bool Polygon::PointOnLine(const Point &firstEdge, const Point &secondEdge, const Point &segmentEdge)
  {
      bool found = false;
      double m = 0;
      double x = firstEdge._x - secondEdge._x;
      int type;
            if(x<0){type=1;}   //lato orientato da sx verso dx
            if(x>0){type=-1;}  //lato orientato da dx verso sx
            if(x==0){type=0;}  //lato verticale

            switch(type){
            case 0:
                if(abs(segmentEdge._x-firstEdge._x)<tolerance) //controllo che abbia la stessa x dei punti del lato
                {
                    if(firstEdge._y<=secondEdge._y)  //controllo che la y di segmentEdge sia compresa tra le y di firstEdge e secondEdge
                     {
                        if((firstEdge._y-segmentEdge._y)<=tolerance && (segmentEdge._y-secondEdge._y)<=tolerance)
                         {
                           found = true;
                         }
                     }
                    else
                    {
                        if((secondEdge._y-segmentEdge._y)<=tolerance && (segmentEdge._y-firstEdge._y)<=tolerance)
                         {
                           found = true;
                         }
                    }
                  }

            break;
            case 1:
                m =(secondEdge._y-firstEdge._y)/(secondEdge._x-firstEdge._x); //coefficiente angolare
                if(abs((segmentEdge._y)-(firstEdge._y + m*(segmentEdge._x-firstEdge._x)))<tolerance) //verifico che punto appartiene a retta
                  {
                    if((firstEdge._x-segmentEdge._x)<=tolerance && (segmentEdge._x-secondEdge._x)<=tolerance) //controllo che la x di segmentEdge sia compresa tra le x di firstEdge e secondEdge
                     {
                        found = true;
                     }
                  }
            break;
              case -1:
                m =(secondEdge._y-firstEdge._y)/(secondEdge._x-firstEdge._x);  //coefficiente angolare
                if(abs((segmentEdge._y)-(firstEdge._y + m*(segmentEdge._x-firstEdge._x)))<tolerance) //verifico che punto appartiene a retta
                {
                    if((secondEdge._x-segmentEdge._x)<=tolerance && (segmentEdge._x-firstEdge._x)<=tolerance) //controllo che la x di segmentEdge sia compresa tra le x di firstEdge e secondEdge
                     {
                        found = true;
                     }
                }
            break;
            }

      return found;
 }


 vector<Point> Polygon::Intersection()
 {
     Vector2d origin;
     Vector2d end;
     int flag=0;
     Point temp = Point();
     origin[0] = segment[0]._x;
     origin[1] = segment[0]._y;
     end[0] = segment[1]._x;
     end[1] = segment[1]._y;

     Intersector1D1D::SetFirstSegment(origin,end); //setto il segmento
     for (unsigned int i=0; i<points.size();i++)
     {
         origin[0] = points[i]._x;
         origin[1] = points[i]._y;
         end[0] = points[(i+1)%points.size()]._x;
         end[1] = points[(i+1)%points.size()]._y;

         Intersector1D1D::SetSecondSegment(origin,end); //origin e end sono due vertici consecutivi del poligono (lato)
         if( Intersector1D1D::ComputeIntersection()==true )
         {
             temp = Intersector1D1D::PointOfIntersection(origin,end); //punto di intersezione segmento/lato
             if(intersections.size()>0)  //controllo che il punto non sia già stato trovato come intersezione in caso corrisponda ad un punto del poligono
             {
                    if(temp==intersections[intersections.size()-1]){flag=1;}
             }
             if(flag==0){intersections.push_back(temp);}
         }
         flag=0;
     }
     unsigned int counter = 0;
     for(unsigned int j = 0; j < segment.size(); j++)  //implemento un algoritmo di ray-casting point in polygon
     {
         origin[0] = segment[j]._x;
         origin[1] = segment[j]._y;
         end[0] = INF;
         end[1] = segment[j]._y;

         Intersector1D1D::SetFirstSegment(origin,end); //setto il primo segmento orizzontale passante per segment[j]

     for (unsigned int i=0; i<points.size();i++)
     {
         origin[0] = points[i]._x;
         origin[1] = points[i]._y;
         end[0] = points[(i+1)%points.size()]._x;
         end[1] = points[(i+1)%points.size()]._y;

         Intersector1D1D::SetSecondSegment(origin,end); //origin e end sono due vertici consecutivi del poligono (lato)
         if( Intersector1D1D::ComputeIntersection()==true && Intersector1D1D::PointOfIntersection(origin,end)._x > segment[j]._x) //verifico quante intersezioni ci sono tra i due segmenti, considerando solo quelle di ascissa > segment[j]
         {
             counter++;
         }

     }
     if(counter%2 != 0)  //se ho un numero dispari di intersezione, il punto è all'interno del poligono quindi lo aggiungo a intersections
         intersections.push_back(segment[j]);
     counter = 0;
     }

    sort(intersections.begin(),intersections.end()); //le riordino perchè ho aggiunto i vertici del segmento
    return intersections;
 }

 vector<Point> Polygon::NewPoints()  //i punti del poligono erano gia stati inseriti in CreatePolygon
 {
     for(unsigned int i=0; i<intersections.size();i++)
     {
        bool found = 0;
        for(unsigned int j=0; j<points.size();j++) //verifico che un punto di intersezione non sia un vertice che esiste già
        {
            if (intersections[i]==points[j])
            {
                found = 1;
            }
        }
        if(found == 0)
          {
            newPoints.push_back(intersections[i]); //lo aggiungo ai nuovi punti
          }
     }
     return newPoints;
 }



 vector<Point> Polygon::OrderOnPolygon()
  {
      int flag=0;
      for (unsigned int i=0; i<points.size(); i++)
      {
          if(perimeterPoints.size()>0) //verifico che il punto non sia già stato aggiunto (nel caso un'intersezione coincida con un vertice)
          {
            if(points[i]==perimeterPoints[perimeterPoints.size()-1]){flag=1;}
          }
          if(flag==0){perimeterPoints.push_back((points[i]));}
          flag=0;
          for(unsigned int j=0; j<intersections.size(); j++)
          {
              if(PointOnLine(points[i], points[(i+1)%(points.size())], intersections[j]) == true) //verifico che l'intersezione appartenga al lato
               {
                  if(perimeterPoints.size()>0) //verifico che il punto non sia già stato aggiunto (nel caso un'intersezione coincida con un vertice)
                  {
                      if(intersections[j]==perimeterPoints[perimeterPoints.size()-1]){flag=1;}
                  }
                  if(flag==0){perimeterPoints.push_back((intersections[j]));}
               }
          }
          flag=0;
      }

     return perimeterPoints;
 }

 bool Polygon::CrossProduct(const Point &p1)
 {
     bool right;
     Matrix2d edges;
     edges(0,0) = segment[1]._x-segment[0]._x;
     edges(1,0) = segment[1]._y-segment[0]._y;
     edges(0,1) = p1._x-segment[0]._x;
     edges(1,1) = p1._y-segment[0]._y;
     if(edges.determinant()<0){right = true;} //arrivo da destra
     else{right = false;} //arrivo da sinistra
     return right;
 }

 vector<int> Polygon::OrderVerticesOnPolygon()
 {
     int flag=0;
     int count=0;
     int temp=0;
     int numVert=points.size(); //parto da points.size() a numerare le intersezioni perchè i vertici del poligono sono già numerati

        //numero i vertici delle intersezioni
        for(unsigned int i =0; i<intersections.size();i++)
        {
            numVert = numVert+i;
            for(unsigned int j =0; j<points.size();j++)  //controllo se l'intersezione coincide con un vertice
            {
                if(intersections[i]==points[j]){flag=1, temp = j;}
            }
            if(flag==1)
            {
                intersectionVertices.push_back(polygonVertices[temp]); //assegno all'intersezione lo stesso vertice del punto a cui è uguale
                numVert = numVert - 1;
            }
            else
            {
                intersectionVertices.push_back(numVert);
            }
            numVert = numVert - i;
            flag=0;
        }
        //riempio il vettore con i vertici ordinati sul perimetro
        for(unsigned int i=0; i<perimeterPoints.size();i++)
        {
           temp=0;
           flag=0;
           for(unsigned int j=0; j<intersections.size();j++) //controllo se il punto è un'intersezione, se lo è ne salvo la position
           {
               if(perimeterPoints[i]==intersections[j]){count=1,temp=j;}
           }
           for(unsigned int k=0; k<points.size() && count != 1; k++) //se il punto non è un'intersezione, ne trovo la position in points
           {
               if(perimeterPoints[i]==points[k]){temp=k;}
           }
           if(perimeterVertices.size()>0) //verifico che il vertice non sia già stato aggiunto
           {
               if(count == 1)
               {
                   if(intersectionVertices[temp]==perimeterVertices[perimeterVertices.size()-1]){flag=1;}
               }
               else
               {
                   if(polygonVertices[temp]==perimeterVertices[perimeterVertices.size()-1]){flag=1;}
               }

           }
           if(flag==0) //se non è ancora stato aggiunto, lo aggiungo
           {
               if(count == 1)
               {
                   perimeterVertices.push_back(intersectionVertices[temp]);
               }
               else
               {
                   perimeterVertices.push_back(polygonVertices[temp]);
               }
           }
           count = 0;

        }
        return perimeterVertices;
 }


  vector<vector<int> > Polygon::CutPolygon()
 {
     int position = 0;  //vertice da cui inizia un poligono
     vector<int> polygon;  //vettore d'appoggio in cui salvo il poligono tagliato
     polygon.clear();
     vector<int> copy;
     copy.clear();
     bool stop=false;
     int check = 0;
     bool newVertex=true;

         for(unsigned int i = 0; i <perimeterPoints.size() && stop!=true;i++)
         {
             position=i;
             while((i+1)%perimeterPoints.size()!=position){  //aggiungo i vertici al nuovo poligono finchè non torno al punto di partenza
             polygon.push_back(perimeterVertices[i]);
             for(unsigned int j = 0; j < intersections.size(); j ++)
             {
                 if(perimeterPoints[i] == intersections[j])  //verifico se il punto è un'untersezione
                 {
                     if(CrossProduct(perimeterPoints[i-1])==true) //controllo se il punto precedente è a destra del segmento
                     {
                         bool onPerimeter = false;
                       for(unsigned int k = j+1 ; k < intersections.size() && onPerimeter ==false; k ++) //percorro il vettore intersections in ordine finchè non torno sul perimetro (a scendere)
                       {
                           polygon.push_back(intersectionVertices[k]);
                           for(unsigned int l = 0; l < perimeterPoints.size(); l++)
                           {
                               if(perimeterPoints[l] == intersections[k]) {onPerimeter=true, i=l;}//pongo i=l per riprendere il ciclo a partire dalla posizione d'uscita
                               if(onPerimeter==true)
                               {
                                   if(perimeterPoints[position]==perimeterPoints[(i)%perimeterPoints.size()]){check=1; break;} //verifico se sono tornato al punto di partenza
                                   if(check==0 && CrossProduct(perimeterPoints[i+1%perimeterPoints.size()])==false){onPerimeter=false;} //altrimenti verifico se il punto successivo a quello di uscita è a sx del segmento, se lo è continuo su intersections
                                   check=0;

                               }
                           }
                        }
                     }
                     else //controllo se il punto precedente è a sinistra del segmento
                     {
                         bool onPerimeter = false;
                         for(unsigned int k = j-1 ; k>=0 && onPerimeter ==false; k --) //percorro il vettore intersections in ordine inverso finchè non torno sul perimetro (a salire)
                         {
                             polygon.push_back(intersectionVertices[k]);
                             for(unsigned int l = 0; l < perimeterPoints.size(); l++)
                             {
                                 if(perimeterPoints[l] == intersections[k]) {onPerimeter=true, i=l;}//pongo i=l per riprendere il ciclo a partire dalla posizione d'uscita
                                 if(onPerimeter==true)
                                 {
                                     if(perimeterPoints[position]==perimeterPoints[(i)%perimeterPoints.size()]){check=1;} //verifico se sono tornato al punto di partenza

                                     if(check==0 && CrossProduct(perimeterPoints[i+1%perimeterPoints.size()])==true){onPerimeter=false;}//altrimenti verifico se il punto successivo a quello di uscita è a dx del segmento, se lo è continuo su intersections
                                     check=0;
                                 }
                             }
                         }
                     }
                    break;
                 }

             }

              i++;
              if(perimeterPoints[position]==perimeterPoints[i%perimeterPoints.size()]) //controllo se il vertice successivo è quello di partenza, se lo è torno indietro di uno in modo da uscire con la condizione del while
              {
                  i=i-1;
              }
              else{  //se è il penultimo vertice del poligono, aggiungo anche l'ultimo vertice perche poi esco dal ciclo
              if((i+1)%perimeterPoints.size()==position){polygon.push_back(perimeterVertices[i]);}
              }

            }
            cuttedPolygons.push_back(polygon); //ho tagliato un nuovo poligono quindi lo aggiungo a cuttedPolygons
            for(unsigned int r=0;r<polygon.size();r++) //copio ogni vertice dei poligoni già tagliati in un vettore d'appoggio
            {
                copy.push_back(polygon[r]);
            }
            for(unsigned int s=0;s<perimeterVertices.size();s++) //controllo se lo s-esimo vertice non è stato ancora aggiunto
            {
                newVertex = true;
                for(unsigned int t=0;t<copy.size();t++)
                {
                    if(perimeterVertices[s]==copy[t]){newVertex=false;}
                }
                if(newVertex==true) // nuovo vertice di partenza trovato
                {
                    i=s-1;
                    break;
                }
            }
            if(newVertex== false){stop=true;}  //se non trovo un nuovo vertice vuol dire che ho finito tutti i vertici
            polygon.clear();
            position=0;

         }
        return cuttedPolygons;
     }
 }

