#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      Point() {X = 0.0, Y = 0.0; } //costruttore default che mette a zero
      Point(const double& x,
            const double& y) { X = x ,Y = y; } // costruttore
      Point(const Point& point) { X = point.X, Y = point.Y ; } //costruttore copia

      double ComputeNorm2() const { return sqrt((X * X) + (Y * Y)); }

      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point)
      {
        stream << "Point: x = " <<point.X<< " y = " <<point.Y <<endl;
        return stream;
      }
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0; //non siimplementa perche virtuale
      virtual void AddVertex(const Point& point) = 0;
      friend inline bool operator< (const IPolygon& lhs, const IPolygon& rhs){if(lhs.Perimeter() < rhs.Perimeter()) return true; else return false; }
      friend inline bool operator> (const IPolygon& lhs, const IPolygon& rhs){if(lhs.Perimeter() > rhs.Perimeter()) return true; else return false; }
      friend inline bool operator<=(const IPolygon& lhs, const IPolygon& rhs){if(lhs.Perimeter() <= rhs.Perimeter()) return true; else return false; }
      friend inline bool operator>=(const IPolygon& lhs, const IPolygon& rhs){if(lhs.Perimeter() >= rhs.Perimeter()) return true; else return false; }

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() {_center.X= 0.0, _center.Y= 0.0, _a = 0.0; _b = 0.0 ;}
      Ellipse(const Point& center,
              const double& a,
              const double& b) {_center.X = center.X ,_center.Y= center.Y, _a = a, _b=b ; }
      virtual ~Ellipse() { }
      void AddVertex(const Point& point) { } //ellisse non ha vertici
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      Circle(): Ellipse() {};
      Circle(const Point& center,
             const double& radius):Ellipse(center,radius,radius) { } // : = figlio cerchio che chiama padre ellisse
      virtual ~Circle() { }
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() {points.resize(3); points[points.size()].X = points[points.size()].X = {0.0};} //faccio unresize perche so che un triangolo ha tre vertici
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point) { points.push_back(point);}
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      TriangleEquilateral(const Point& p1,
                          const double& edge);
  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() {points.resize(4); points[points.size()].X = points[points.size()].X = {0.0}; } //come triangolo
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p) {points.push_back(p); }

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      Rectangle(): Quadrilateral() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4): Quadrilateral(p1,p2,p3,p4) {}
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
  };

  class Square: public Rectangle
  {
    public:
      Square(): Rectangle() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4): Rectangle(p1,p2,p3,p4) {}
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
  };
}

#endif // SHAPE_H
