#include "shape.h"
#include <math.h>

namespace ShapeLibrary {

  Point Point::operator+(const Point &point) const
  {
    return Point(X + point.X, Y + point.Y);

  }

  Point Point::operator-(const Point &point) const
  {
      return Point(X - point.X, Y - point.Y);
  }

  Point &Point::operator-=(const Point &point)
  {
      X-=point.X , Y-= point.Y; return *this;
  }

  Point &Point::operator+=(const Point &point)
  {
       X+=point.X , Y+= point.Y; return *this;
  }



  double Ellipse::Perimeter() const
  {
      return (2*M_PI*sqrt(((_a*_a)+ (_b*_b))/2)); //perimetro ellisse
  }


  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
  }

  double Triangle::Perimeter() const
  {
      unsigned int i,lunghezza;
      double perimeter = 0;

      for( i=0; i<points.size(); i++)
        {
            lunghezza=points.size();
            perimeter+=sqrt((points[i%lunghezza].X-points[(i+1)%lunghezza].X)*(points[i%lunghezza].X-points[(i+1)%lunghezza].X)+
                    (points[i%lunghezza].Y-points[(i+1)%lunghezza].Y)*(points[i%lunghezza].Y-points[(i+1)%lunghezza].Y));

        }

    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1,const double& edge):Triangle(p1,p1,p1)
  {
    points[1].X = points[0].X-(0.5 * edge); //p1=points[0] lo considero come il vertice del triangolo equilatero e points[1] e points[2] la base
    points[1].Y = points[0].Y -(0.5 * sqrt(3) * edge);
    points[2].X = points[0].X + (0.5 * edge);
    points[2].Y = points[1].Y;
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      AddVertex(p1);
      AddVertex(p2);
      AddVertex(p3);
      AddVertex(p4);
  }

  double Quadrilateral::Perimeter() const
  {
      unsigned int i,lunghezza;
      double perimeter = 0;

      for( i=0; i<points.size(); i++)
        {
            lunghezza=points.size();
            perimeter+=sqrt((points[i%lunghezza].X-points[(i+1)%lunghezza].X)*(points[i%lunghezza].X-points[(i+1)%lunghezza].X)+
                    (points[i%lunghezza].Y-points[(i+1)%lunghezza].Y)*(points[i%lunghezza].Y-points[(i+1)%lunghezza].Y));

        }



    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1,const double& base,const double& height):Quadrilateral(p1,p1,p1,p1)
  {
     points[1].X = points[0].X + base; //p2 x
     points[1].Y = points[0].Y; // p2 y
     points[2].X = points[1].X; // p3 x
     points[2].Y = points[0].Y + height;  //p3 y
     points[3].X = points[0].X; //p4 x
     points[3].Y = points[2].Y; //p4 y

  }

  Square::Square(const Point &p1, const double &edge):Rectangle(p1,p1,p1,p1)
  {
      points[1].X = points[0].X + edge; //p2 x
      points[1].Y = points[0].Y; // p2 y
      points[2].X = points[1].X; // p3 x
      points[2].Y = points[0].Y + edge;  //p3 y
      points[3].X = points[0].X; //p4 x
      points[3].Y = points[2].Y; //p4 y
  }



 }
